$(() => {
  const butterfly1 = grid3x3[0][0]
  const butterfly9 = grid3x3[2][2]
  const g1 = tap(butterfly1.position, moveAway, grid3x3)
  const g2 = tap(butterfly1.position, moveCloser, g1)

  // console.log('grid3x3', grid3x3)
  // console.log('g1', g1)
  // console.log('g2', g2)
  
  // render(g)
  render(grid3x3)
})

let clickFlag = true

const stepSize = 1
// (%)
const distance = 10

// String -> Number -> Number -> Tile
const makeTile = (src, x, y) => ({
  src: src,
  position: {
    x: x,
    y: y
  }
})

// Number -> Number -> Tile
const makeButterfly = (x, y) => makeTile('Butterfly.svg', x, y)

// $Tile
const $makeTile = (tile, grid) => {
  const x = (50 + (tile.position.x * distance)) + 'vw'
  const y = (50 + (tile.position.y * distance)) + 'vh'
  const f = clickFlag ? moveAway : moveCloser
  const clickHandler = () => {
    render(tap(tile.position, f, grid))
  }
  
  return $('<img>')
    .attr('src', tile.src)
    .addClass('butterfly')
    .css('left', x)
    .css('top', y)
    .click(clickHandler)
}

// Position -> Tile -> Tile
const moveAway = (position, tile) => {
  const tileX = tile.position.x + stepSize * (tile.position.x - position.x)
  const tileY = tile.position.y + stepSize * (tile.position.y - position.y)
  return makeTile(tile.src, tileX, tileY)
}

// Position -> Tile -> Tile
const moveCloser = (position, tile) => {
  const tileX = tile.position.x - stepSize * (tile.position.x - position.x)
  const tileY = tile.position.y - stepSize * (tile.position.y - position.y)
  return makeTile(tile.src, tileX, tileY)
}

// Position -> (Position -> Tile -> Tile) -> Grid -> Grid
const tap = (position, f, grid) => grid.map(row => row.map(tile => f(position, tile)))

// [[Tile]]
const grid3x3 = [
    [makeButterfly(-1, -1), makeButterfly(0, -1), makeButterfly(1, -1)],
    [makeButterfly(-1, 0), makeButterfly(0, 0), makeButterfly(1, 0)],
    [makeButterfly(-1, 1), makeButterfly(0, 1), makeButterfly(1, 1)]
]

const render = grid => {
  const $grid = grid.map(row => row.map(tile => $makeTile(tile, grid)))

  console.log('grid:', grid)
  console.log('$grid:', $grid)
  clickFlag = !clickFlag

  $('#main').empty()
  
  $grid.forEach(row => {
    row.forEach($tile => {
      $('#main').append($tile)
    })
  })
}
