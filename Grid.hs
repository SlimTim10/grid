type Position = (Int, Int)
type Tile = (String, Position)
type Grid = [[Tile]]

stepSize :: Int
stepSize = 2

grid3x3 :: Grid
grid3x3 =
  [ [("Butterfly1", (-1, -1)), ("Butterfly2", (0, -1)), ("Butterfly3", (1, -1))]
  , [("Butterfly4", (-1, 0)), ("Butterfly5", (0, 0)), ("Butterfly6", (1, 0))]
  , [("Butterfly7", (-1, 1)), ("Butterfly8", (0, 1)), ("Butterfly9", (1, 1))]
  ]

moveAway :: Position -> Tile -> Tile
moveAway (x, y) (name, (tileX, tileY)) = (name, (tileX', tileY'))
  where
    tileX' = tileX + stepSize * (signum (tileX - x))
    tileY' = tileY + stepSize * (signum (tileY - y))

moveCloser :: Position -> Tile -> Tile
moveCloser (x, y) (name, (tileX, tileY)) = (name, (tileX', tileY'))
  where
    tileX' = tileX - stepSize * (signum (tileX - x))
    tileY' = tileY - stepSize * (signum (tileY - y))

tap :: Position -> (Position -> Tile -> Tile) -> Grid -> Grid
tap pos f = map (map (f pos))

main = do
  let grid = grid3x3
  print $ grid
  let grid2 = tap (-1, -1) moveAway grid
  print $ grid2
  let grid3 = tap (-1, -1) moveCloser grid2
  print $ grid3
